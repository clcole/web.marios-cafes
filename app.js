
const form = document.querySelector("#add-cafe-form");
const cafeList = document.querySelector("#cafe-list");

function renderCafe(doc) {
  const li = document.createElement("li");
  li.setAttribute("data-id", doc.id);

  const name = document.createElement("span");
  name.textContent = doc.data().name;
  li.appendChild(name);

  const city = document.createElement("span");
  city.textContent = doc.data().city;
  li.appendChild(city);

  const deleteItem = document.createElement("div");
  deleteItem.textContent = "x";
  deleteItem.addEventListener("click", (e) => onCafeDelete(e));
  li.appendChild(deleteItem);

  cafeList.appendChild(li);
}

// add cafe to the db
function onCafeAdd(e) {
  e.preventDefault();

  db.collection("cafes").add({
    name: form.name.value,
    city: form.city.value
  });

  form.name.value = "";
  form.name.focus();
  form.city.value = "";
}
form.addEventListener("submit", (e) => onCafeAdd(e));

// delete cafe from the db
function onCafeDelete(e) {
  const id = e.target.parentElement.getAttribute("data-id");
  db.collection("cafes").doc(id).delete();
}

// remove cafe from the list
function removeCafe(doc) {
  const li = cafeList.querySelector(`[data-id="${doc.id}"]`);
  cafeList.removeChild(li);
  // or
  // li.parentNode.removeChild(li);
}

// real-time database listener
db.collection("cafes").onSnapshot(snapshot => {

  snapshot.docChanges().forEach(change => {
    if (change.type === "removed") {
      removeCafe(change.doc);
    }

    if (change.type === "added") {
      renderCafe(change.doc);
    }
  });

});

// single database call
// db.collection("cafes").orderBy("name").get().then(snapshot => {
//   snapshot.docs.forEach(doc => {
//     renderCafe(doc);
//   });
// });